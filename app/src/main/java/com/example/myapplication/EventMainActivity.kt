package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView

class EventMainActivity : AppCompatActivity() {

    private var RETOUR: String? = null
    private val TAG = "EventMainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_main)


    }

        fun newPage(view: View) {

            val progressbar = findViewById<ProgressBar>(R.id.progressbar)

            if (RETOUR == null) {
                val intent = Intent(this, EventDetailActivity::class.java)
                startActivityForResult(intent, 123)
                progressbar.visibility = View.VISIBLE

            } else {
                val intent = Intent(Intent.ACTION_SEND)
                intent.putExtra(Intent.EXTRA_TEXT, RETOUR)
                intent.type = "text/plain"

                if (intent.resolveActivity(packageManager) != null)
                {
                    startActivity(intent)
                }
            }
        }

    override fun onResume() {
        super.onResume()
        if (RETOUR != null){
            var word=findViewById<TextView>(R.id.wordToSend)
            word.text = RETOUR
        }
    }
        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
        {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 123 && resultCode == RESULT_OK && data != null)
            {
                RETOUR = data.getStringExtra("ExtraRetour")
            }
        }


}