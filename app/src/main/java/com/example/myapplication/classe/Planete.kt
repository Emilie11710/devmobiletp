package com.example.myapplication.classe

class Planete {
    var name: String? = null
    var rotation_period = 0
    var orbital_period = 0
    var diameter = 0
    var climate: String? = null
    var gravity: String? = null
    var terrain: String? = null
    var surface_water: String? = null
    var population: String? = null

    override fun toString(): String {
        return "Planete(name='$name', rotation_period=$rotation_period, orbital_period=$orbital_period, diameter=$diameter, climate='$climate', gravity='$gravity', terrain='$terrain', surface_water=$surface_water, population=$population)"
    }


}