package com.example.myapplication.metier.bdd

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.metier.dao.MemoDAO
import com.example.myapplication.metier.dto.MemoDTO

@Database(entities = [MemoDTO::class], version = 1)

abstract class AppDatabase : RoomDatabase()
{
    abstract fun memosDAO(): MemoDAO
}