package com.example.myapplication.metier.dao

import androidx.room.*
import com.example.myapplication.metier.dto.MemoDTO

@Dao
abstract class MemoDAO {

    @Query("SELECT * FROM memos")
    abstract fun getListeMemos(): List<MemoDTO>

    @Query("SELECT COUNT(*) FROM memos WHERE intitule = :intitule")
    abstract fun countMemosParIntitule(intitule: String): Long

    @Insert
    abstract fun insert(vararg memos: MemoDTO)

    @Update
    abstract fun update(vararg memos: MemoDTO)

    @Delete
    abstract fun delete(vararg memos: MemoDTO)
}