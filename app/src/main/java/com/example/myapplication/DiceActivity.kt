package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlin.random.Random

class DiceActivity : AppCompatActivity() {

    private var dice1: Int = 0
    private var dice2: Int = 0
    private var dice3: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dice)

        if (savedInstanceState != null)
        {
            dice1 = savedInstanceState.getInt("dice1")
            dice2 = savedInstanceState.getInt("dice2")
            dice3 = savedInstanceState.getInt("dice3")
        }
        else {
            dice1 =  Random.nextInt(1, 6)
            dice2 =  Random.nextInt(1, 6)
            dice3 =  Random.nextInt(1, 6)
        }


        var dice1Text=findViewById<TextView>(R.id.dice1)
        dice1Text.text = dice1.toString()

        var dice2Text=findViewById<TextView>(R.id.dice2)
        dice2Text.text = dice2.toString()

        var dice3Text=findViewById<TextView>(R.id.dice3)
        dice3Text.text = dice3.toString()
    }

    override fun onSaveInstanceState(outState: Bundle) {

        super.onSaveInstanceState(outState)

        outState.putInt("dice1", dice1)
        outState.putInt("dice2", dice2)
        outState.putInt("dice3", dice3)
    }



}