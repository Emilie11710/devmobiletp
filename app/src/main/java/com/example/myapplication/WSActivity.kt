package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import android.view.View
import com.example.myapplication.classe.Planete
import com.example.myapplication.classe.Planetes
import com.example.myapplication.ws.RetrofitSingleton
import com.example.myapplication.ws.WSInterface
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random

class WSActivity : AppCompatActivity() {

    private lateinit var planeteToShow: Planete

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wsactivity)
    }

    fun getplanetRandom(view: View) {

        val service = RetrofitSingleton.retrofit.create(WSInterface::class.java)
        val call = service.getAllPlanets()

        call.enqueue(object: Callback<Planetes> {

            override fun onResponse(
                call: retrofit2.Call<Planetes>,
                response: Response<Planetes>
            ) {
                if (response.isSuccessful)
                {
                    val planetes = response.body()
                    val randomId = Random.nextInt(1, 60)



                    planeteToShow = planetes!!.results!!.get(randomId-1);

                    Log.d("tag", "planeteToShow")


                }
            }

            override fun onFailure(call: retrofit2.Call<Planetes>, t: Throwable) {
                Log.e("tag", "${t.message}")
            }

        })
    }
}