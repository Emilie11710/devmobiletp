package com.example.myapplication


import android.os.Bundle
import android.widget.Button
import android.widget.ScrollView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class Liseuse : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_liseuse)

       Toast.makeText(this, "Bienvenue !", Toast.LENGTH_LONG).show()

        val back_to_top = findViewById<Button>(R.id.retour)
        val text_scrollview = findViewById<ScrollView>(R.id.scroll)
        back_to_top.setOnClickListener {
            Toast.makeText(this, "Vous êtes revenu au début de l'article !", Toast.LENGTH_SHORT).show()
            text_scrollview.fullScroll(ScrollView.FOCUS_UP)
        }
    }
}