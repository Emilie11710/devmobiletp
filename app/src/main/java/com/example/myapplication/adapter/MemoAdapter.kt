package com.example.myapplication.adapter

import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.classe.Memo
import com.example.myapplication.metier.dto.MemoDTO

private const val TAG = "MemoAdapter"

class MemoAdapter (private var listeMemos: MutableList<MemoDTO>) :
RecyclerView.Adapter<MemoAdapter.MemoViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MemoViewHolder {
        val viewMemo = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_memo, parent, false)
        return MemoViewHolder(viewMemo)
    }

    override fun onBindViewHolder(holder: MemoViewHolder, position: Int) {
        holder.textViewLibelleMemo.text = listeMemos[position].intitule
    }

    override fun getItemCount(): Int = listeMemos.size

    fun updateMemos(listeMemos: MutableList<MemoDTO>){
        this.listeMemos = listeMemos
        notifyDataSetChanged()
    }

    inner class MemoViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        val textViewLibelleMemo: TextView = itemView.findViewById(R.id.libelle_memo)
        init
        {
            textViewLibelleMemo.setOnClickListener {
                val memoDTO = listeMemos[adapterPosition]
                Log.d(TAG, "${listeMemos.indexOf(memoDTO)}: ")
                //Toast.makeText(itemView.getContext(), "${memoDTO.intitule}", Toast.LENGTH_LONG).show()
                
                val preferences = PreferenceManager.getDefaultSharedPreferences(itemView.getContext())
                val editor = preferences.edit()
                editor.putString("lastClick", "dernierClick en position : ${listeMemos.indexOf(memoDTO)+ 1}")
                editor.apply()
            }
        }
    }
}