package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.adapter.MemoAdapter
import com.example.myapplication.classe.Memo
import com.example.myapplication.fragments.DetailFragment
import com.example.myapplication.fragments.EXTRA_MEMO
import com.example.myapplication.metier.bdd.AppDatabaseHelper
import com.example.myapplication.metier.dto.MemoDTO

class ListActivity : AppCompatActivity() {

    private lateinit var memoAdapter: MemoAdapter
    private lateinit var recyclerViewMemo: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        //recycleView
        recyclerViewMemo = findViewById(R.id.liste_memos)

        // à ajouter pour de meilleures performances :
        recyclerViewMemo.setHasFixedSize(true)

        // layout manager, décrivant comment les items sont disposés :
        val layoutManager = LinearLayoutManager(this)
        recyclerViewMemo.layoutManager = layoutManager

        // contenu :
        val listeMemos: List<MemoDTO> =  AppDatabaseHelper.getDatabase(this).memosDAO().getListeMemos()

        // adapter :
        memoAdapter = MemoAdapter(listeMemos.toMutableList())
        recyclerViewMemo.adapter = memoAdapter


        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val valeur = preferences.getString("lastClick", null)
        
        if (valeur != null) {
            Toast.makeText(this, "$valeur", Toast.LENGTH_LONG).show()
        }
// fragment :
        val fragment = DetailFragment()

        // parametre vers le fragment
        val bundle = Bundle()
        bundle.putString(EXTRA_MEMO, "Ceci est un mémo")
        fragment.arguments = bundle

        // transaction :
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.conteneur_fragment, fragment, "exemple2")
        transaction.commit()

        
    }

    fun addToMemoList(view: View) {
        val editTextMemo: EditText = findViewById(R.id.saisie_memo)
        val saisieMemo = editTextMemo.text.toString()
        val memo = Memo(saisieMemo)
        val memoDTO = MemoDTO(0, saisieMemo)

        // ajout
        AppDatabaseHelper.getDatabase(this).memosDAO().insert(memoDTO)
        // contenu :
        val listeMemos: List<MemoDTO> =  AppDatabaseHelper.getDatabase(this).memosDAO().getListeMemos()
        memoAdapter.updateMemos(listeMemos.toMutableList())
        //memoAdapter.addMemo(memo)

        editTextMemo.setText("")
        recyclerViewMemo.smoothScrollToPosition(0)
    }

    fun seeMemoDetail(view: View) {

    }
}


