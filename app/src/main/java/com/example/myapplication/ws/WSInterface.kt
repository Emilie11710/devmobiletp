package com.example.myapplication.ws

import com.example.myapplication.classe.Planete
import com.example.myapplication.classe.Planetes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WSInterface {

    // appel get :
    @GET("api/planets/")
    fun getAllPlanets(): Call<Planetes>
}