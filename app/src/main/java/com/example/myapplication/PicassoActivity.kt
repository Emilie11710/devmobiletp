package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import com.squareup.picasso.Picasso

class PicassoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picasso)

        val imageView: ImageView = findViewById(R.id.image)
        Picasso.get()
            .load("https://www.le-tout-lyon.fr/content/images/2020/01/16/11958/thumbnailpicassobuste-de-femmemcclay-gallery.jpg")
            .fit()
            .centerInside()
            .into(imageView)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean
    {
        menuInflater.inflate(R.menu.demo_menu, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return when (item.itemId)
        {
           R.id.action_favori ->
            {
                Log.i("tag", "favori")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}