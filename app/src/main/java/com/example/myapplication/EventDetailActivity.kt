package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView

class EventDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event_detail)
    }

    fun sendValue() {


    }

    fun sendValue(view: View) {
        var word=findViewById<EditText>(R.id.word).text.toString()

        val intent = Intent()
        intent.putExtra("ExtraRetour", word)
        setResult(RESULT_OK, intent)
        finish()
    }
}


